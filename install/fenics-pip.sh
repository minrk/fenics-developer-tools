#!/bin/bash
set -e
if [ "x${FENICS_INSTALL_PREFIX}" = "x" ]; then
	echo Please set $FENICS_INSTALL_PREFIX to your installation prefix.
    exit -1
else
    PIP=pip$FENICS_PYTHON_VERSION
    if hash $PIP > /dev/null 2>&1; then
        while [ ! -e setup.py ]; do
            cd ..
            if [ `pwd` = '/' ]; then
                echo Found no setup.py in parent paths!
                exit -1
            fi
        done

        $PIP install \
             --no-deps --upgrade \
             --cache-dir=${FENICS_INSTALL_PREFIX}/pipcache \
             --install-option='--prefix='${FENICS_INSTALL_PREFIX}'' \
             .
    else
        echo Cannot find $PIP to match $FENICS_PYTHON_EXECUTABLE.
        exit -1
    fi
fi
